## Context
Cr�er une librairie java qui partitionne une liste selon la taille de block

## Artifact
      <dependency>
	    <groupId>adneom</groupId>
    	<artifactId>partitionCalculator</artifactId>
    	<version>1.0.0</version>
      <dependency>

## Exemple

`   public void generateBlocksView() {
        var blocks=PartitionCalc.partition(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9), 3);
        System.out.println(blocks);
     }`

Resultat:

` [[1, 2, 3], [4, 5, 6], [7, 8, 9]]`

## Packaging : Jar
## Version Java minimum : 10