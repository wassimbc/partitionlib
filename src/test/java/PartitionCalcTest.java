
import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.List.of;
import static org.junit.Assert.assertEquals;


public class PartitionCalcTest  {


    @Test(expected = IllegalArgumentException.class)
    public void partitionnate_nullInput(){
        assertEquals(List.of(new ArrayList<Integer>()),PartitionCalc.partition(null,1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void partitionnate_zeroIndex(){
        assertEquals(List.of(new ArrayList<Integer>()),PartitionCalc.partition(List.of('A'),0));
    }

    @Test
    public void partitionnate_outOfBound(){
        List<Character> input= List.of('1', 'A' ,'5');
        assertEquals(List.of(input),PartitionCalc.partition(input,6));
    }

    @Test
    public void partitionnate_nominal(){
        List<Character> input= List.of('1', 'A' ,'5');
        List<List<Character>> expectedResult= List.of(List.of('1'),List.of('A'),List.of('5'));
        assertEquals(expectedResult,PartitionCalc.partition(input,1));
    }


}
